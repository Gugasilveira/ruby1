# atribuindo valores a array inicial
seq = [3, 6, 7, 8]
# definindo uma nova array sem nada para receber o contador
nova = []
contador = 0
# criando um laço que ira percorrer toda a array inicial
seq.each do |num|
# criando uma condicional na qual se o resto da divisão de um numero da array for zero ele é adicionado na nova array em sua respectiva posição que o contador marcar    
    if (num % 3) == 0
        nova[contador] = num
    end
# terminado cada repetição, é somando mais um numero para mudar a posição que o contador vai indicar dentro do array    
    contador += 1
end
# imprimindo resultado na nova array
print nova